console.log(`JavaScript the Best!`);

// OBJECTS

// a data type that is used to represent real world objects.
// collection of related data and/or functionalities/methods
// information stored are represented in a key:value pair
// key or property
// different data types may be stored

// Creating objects using object initializers/literal notation

// Syntax
//  let objectName = {
// 	keyA: valueA,
// 	keyB: valueB
// }

// creates/declares an object and also initializes/assign its properties upon creation
// A cellphone is an example of real world object
// has own properties

let cellphone = {
  name: "Nokia 3310",
  manufactureDate: 1999,
};
console.log(cellphone);

// CONSTRUCTOR FUNCTION
// creates a reusable function to create several objects that have the same data structure
// useful for creating multiple instances/copies of an object
// instance is a concrete occurrence of any object which emphasize distinct/unique identity of it
// Syntax:
// function objectName(valueA, valueB) {
// 	this.keyA = valueA,
// 	this.keyB = valueB
// }

function Laptop(name, manufactureDate) {
  this.laptopName = name;
  this.laptopManufactureDate = manufactureDate;
}

let laptop1 = new Laptop("Lenovo", 1995);
console.log(laptop1);

// Instantiation
// "new" operator creates an instances of an object
// objects and instances are often interchanged because object literals(let object = {}) and instances (let objectName = new functionName(arguments)) are distinct/unique objects

const laptop2 = new Laptop("MacBook Air", 2020);
console.log(laptop2);

const laptop3 = new Laptop("MacBook Air", 2020);
console.log(laptop3);

// Mini activity
// object menu property: menuName, menuPrice

function Menu(menuName, menuPrice) {
  this.menuName = menuName;
  this.menuPrice = menuPrice;
}

let menu1 = new Menu("Tapsilog", 50);
console.log(menu1);

// creating empty objects
let computer = {};
let myComputer = new Object();

console.log(computer);
console.log(myComputer);

// Accessing objects
let array = [laptop1, laptop2];
console.log(array);
console.log(array[0]);
console.log(array[0]["laptopName"]);
console.log(array[0].laptopName);
console.log(laptop1["laptopManufactureDate"]);

// Initialize/adding/deleting/reassigning Object properties

let car = {};
console.log(car);

// Adding property
car.name = "Toyota";
console.log(car);

car["model"] = "Vios";
console.log(car);

// Deleting property
delete car.model;
console.log(car);

// Reassigning properties
car.name = "Honda";
console.log(car);

car["name"] = "Sarao";
console.log(car);

// OBJECT Methods
// function which is a property of an object
// related to specific objects

// TALK

let person = {
  name: "John",
  talk: function () {
    console.log(`Hello, my name is ${this.name}`);
  },
};
console.log(person);
person.talk();

// WALK
person.walk = function () {
  console.log(`${this.name} walked 25 steps forward.`);
};
person.walk();

let friends = {
  firstName: "Joe",
  lastName: "Smith",
  address: {
    city: "Austin",
    state: "Texas",
  },
  emails: ["joe@mail.com", "joesmith@email.xyz"],
  introduce: function () {
    console.log(
      `Hello, my name is ${this.firstName} ${this.lastName}. I am from ${this.address.city}, ${this.address.state}. You can email me at ${this.emails[0]} or ${this.emails[1]}.`
    );
  },
};
friends.introduce();

// create object instructor
function Pokemon(name, level) {
  this.pokemonName = name;
  this.pokemonLevel = level;
  this.pokemonHealth = 2 * level;
  this.pokemonAttack = level;

  this.tackle = function (targetPokemon) {
    console.log(`${this.pokemonName} tackles ${targetPokemon.pokemonName}`);
    console.log(
      `${targetPokemon.pokemonName}' ${
        targetPokemon.pokemonHealth
      } is now reduced to ${targetPokemon.pokemonHealth - this.pokemonAttack}`
    );
  };
  this.fainted = function () {
    if (targetPokemon.pokemonHealth <= 0) {
      console.log(`${targetPokemon.pokemonName} fainted!`);
    }
  };
}

let pikachu = new Pokemon("Pikachu", 120);
console.log(pikachu);

let gyarados = new Pokemon("Gyarados", 20);
console.log(gyarados);

pikachu.tackle(gyarados);
